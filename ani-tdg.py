#
# Test Data Generator
# V1.2
#
# Author: Andreas Niggemann
#

from datetime import datetime
import sys
import os
import csv
import random
import uuid

field_values = {}
parameters = {}
schema = []

######################################################################
# Base settings: 
# Generate 100 data sets
# With CSV delimiter ;
parameters['numberofsets'] = str(100)
parameters['delimiter'] = ';'
parameters['replacerkey'] = '###'
parameters['useheader'] = 'Y'
parameters['username'] = os.getlogin()
parameters['timeformat'] = '%H:%M:%S'
parameters['dateformat'] = '%Y-%m-%d'
parameters['datetimeformat'] = '%Y-%m-%d_%H:%M:%S'
parameters['timestampformat'] = '%Y-%m-%dT%H:%M:%S.%fZ'
parameters['templatefilename'] = ''
parameters['outputfilename'] = ''
parameters['appendtofile'] = 'N'
######################################################################

schemadelimiter = ';'
parameterkey = '!!!'
constant_uuid = str(uuid.uuid4())
output_file_extension = '.csv'
output_filename_addon_format = '%Y_%m_%d_%H_%M_%S'


def show_help():
    help_text = '''
Test Data Generator
(c) Andreas Niggemann

Parameters:
    Data generator schema filename 
    Example: 
        ani-tdg.py neu_testdata.dgschema

dgschema file content example:
!!!numberofsets;1000
!!!delimiter;";"
!!!useheader;Y

Dataset Name;###setcounter###_of_###numberofsets###_###FN###_###SN###
FN;Steve;Paul;Jack;July;Christin;Mary;Anne;Joe;Myriam;Timothy
SN;Bergman;Cook;Carpenter;Wolf;Shapiro;Kovalsky;Witbury;Omar
Married;Yes;No
Children;;1;2;3;4;5;6;7;8;9;10
run ID;###constantuuid###
set ID;###datasetuuid###
Run Timestamp;###timestamp###
Set Timestamp;###datasettimestamp###

Each line in the dgschema file consists of a name followed by a list 
of possible values. Each name will be translated to a column name 
in the resulting output file. Each cell value will be selected 
randomly from the list of possible values.

Predefined values for replacement:
    ###setcounter### - Contains the current dataset number
    ###numberofsets### - Number of datasets to be generated
    ###delimiter### - Delimiter between values
    ###time### - Current time
    ###date### - Current date
    ###datetime### - Current datetime
    ###timestamp### - Current timestamp
    ###datasettimestamp### - Timestamp per dataset
    ###username### - Current username (login user)
    ###constantuuid### - Unique Identifier for this run
    ###datasetuuid### - Unique Identifier for current dataset
Each value of the current dataset can be referenced for replacement
in the same line (see exmaple above).

Possible generation parameters:
    numberofsets - How many data sets will be generated (100 default)
    delimiter - delimiter between values, ; (semicolon) is default
    replacerkey - Marker string for value replacements, usually ###
    useheader - Y (default) oder N for header line or no header line
    username - Current username depending on operating system login
    timeformat - Format string for time values
    dateformat - Format string for date values
    datetimeformat - Format string for datetime values
    timestampformat - Format string for timestamp values
    templatefilename - Template filename for single file per data set
    outputfilename - Output filename
    appendtofile - N (default) or Y for overwriting or appending
Generation parameters can be set in the dgschema file via 
!!! Annotation (see example above). 
Generation parameters are optional.

A template file can be addressed via templatefilename for outputs like
XML or JSON. The template file may contain column references in the 
usual style (###column name###).
'''
    print(help_text)


def get_random_list_element(actu_list):
    elem = ''
    if len(actu_list) > 0:
        elem = actu_list[random.randint(1, len(actu_list) - 1)]
    return elem


def replacer(field_value):
    fval = field_value
    if fval.find(parameters['replacerkey']) >= 0:
        for key in field_values:
            fval = fval.replace(parameters['replacerkey'] + key + parameters['replacerkey'], field_values[key])
    return fval


def set_sys_field_values(fv, setc):
    fv['setcounter'] = str(setc)
    fv['numberofsets'] = parameters['numberofsets']
    fv['delimiter'] = parameters['delimiter']
    fv['time'] = parameters['time']
    fv['date'] = parameters['date']
    fv['datetime'] = parameters['datetime']
    fv['timestamp'] = parameters['timestamp']
    fv['username'] = parameters['username']
    fv['constantuuid'] = constant_uuid


# Write header
def write_header(with_header, dalists, txfile):
    if with_header:
        headers = ''
        for dl in dalists:
            headers += parameters['delimiter'] + dl[0]
        txfile.write(headers[len(parameters['delimiter']):] + '\n')


# Write datasets with/out headers
def write_sets(output_filename, template_filename, datalists):
    template_content = ''
    text_file = None
    filemode = 'a' if parameters['appendtofile'][0] == 'Y' else 'w'
    if template_filename != '':  # Output via template engine
        if os.path.isfile(template_filename):
            with open(template_filename) as template_file:
                template_content = template_file.read()
    else:  # CSV output
        text_file = open(output_filename, filemode)
        write_header(parameters['useheader'][0] != 'N', datalists, text_file)
    scounter = 0
    for i in range(int(parameters['numberofsets'])):
        scounter += 1
        field_values.clear()
        for dl in datalists:  # Get all field values if they are needed for replacements
            field_values[dl[0]] = get_random_list_element(dl)
        set_sys_field_values(field_values, scounter)
        field_values['datasetuuid'] = str(uuid.uuid4())
        field_values['datasettimestamp'] = datetime.now().strftime(parameters['timestampformat'])
        # Build data set and execute replacements
        if template_content != '':  # Output based on template
            tc = template_content
            current_output_filename = replacer(output_filename)
            field_values['outputfilename'] = current_output_filename
            for key in field_values:
                fv = replacer(field_values[key])
                tc = tc.replace('###' + key + '###', fv)
            with open(current_output_filename, filemode) as outfile:
                outfile.write(tc)
        else:  # CSV Output
            field_values['outputfilename'] = output_filename
            line = ''
            for dl in datalists:
                fv = replacer(field_values[dl[0]])
                line += parameters['delimiter'] + fv
            text_file.write(line[len(parameters['delimiter']):] + '\n')
    if template_content == '':
        text_file.close()


# Read schema file with parameters
def read_schema_file(schemafile, schemalist, parametersdict):
    with open(schemafile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=schemadelimiter, quotechar='"')
        for row in csv_reader:
            if row:
                if row[0].find(parameterkey) == 0:
                    value = row[1].replace("\\t", chr(9)).replace("\\n", chr(13))
                    parametersdict[(row[0][len(parameterkey):])] = value
                else:
                    schemalist.append(row)
        parameters['useheader'] += ' '  # Force at least one character
        parameters['appendtofile'] += ' '  # Force at least one character


def process_dgschema(dgschema_filename):
    read_schema_file(dgschema_filename, schema, parameters)
    now = datetime.now()
    parameters['time'] = now.strftime(parameters['timeformat'])
    parameters['date'] = now.strftime(parameters['dateformat'])
    parameters['datetime'] = now.strftime(parameters['datetimeformat'])
    parameters['timestamp'] = now.strftime(parameters['timestampformat'])
    templatefile = parameters['templatefilename'].strip()
    outputfile = parameters['outputfilename'].strip()
    if outputfile == '':
        filename, file_extension = os.path.splitext(dgschema_filename)
        extrafilename = datetime.now().strftime(output_filename_addon_format)
        outputfile = filename + '_' + extrafilename + output_file_extension
    write_sets(outputfile, templatefile, schema)


#
# Main
#
prg_namebase, prg_ext = os.path.splitext(sys.argv[0])
same_name_schema = prg_namebase + '.dgschema'
if len(sys.argv) > 1:
    arg_file = sys.argv[1]
    if os.path.isfile(arg_file):
        process_dgschema(arg_file)
else:
    if os.path.isfile(same_name_schema):
        process_dgschema(same_name_schema)
    else:
        show_help()
