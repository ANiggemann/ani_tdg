# ANI-TDG
ANI-TDG is a simple test data generator (written in Python) to randomly generate
large chunks of data and output these into a CSV file. 
Or each dataset can be integrated into a separate file via templates.
It can be controlled by a dgschema file that contains the column designations 
and a list of possible values for each column. The number of datasets and some
other parameters can be defined in the same dgschema file.

ANI-TDG Features
-
* One dgschema file for all settings and definitions
* If no command line parameter is given default schema file is ani-tdg.dgschema
* Define the number of datasets to be generated
* Define the field separator (delimiter)
* Each column has a list of possible values that are randomly choosen during CSV generation
* Column header can be switched on or off for the CSV file
* Column values can reference other columns for replacements in the same dataset
* Predefined symbols for datetime, dataset counter, username, uuids etc.
* Individual output formats for date, datetime, time and timestamp can be defined
* Set output filename in dgschema file (optional)
* Output filename can be automatically generated from the dgschema filename and adding date and time
* Dataset output can be to separate files via template file
* Drag & drop dgschema files to the Python script to start 

Installation
-
* Copy the file ani-tdg.py to an arbitrary directory
* Python must be in your path and associated with the file extension py

Usage
-
Parameters:<br>
&nbsp;&nbsp;&nbsp;&nbsp;Data generator schema filename<br>
&nbsp;&nbsp;&nbsp;&nbsp;Example:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ani-tdg.py testdata.dgschema<br>
<br>
Or drag & drop the dgschema file onto ani-tdg.py<br>
<br>
dgschema file content example:<br>
```
!!!numberofsets;1000
!!!delimiter;";"
!!!useheader;Y

Dataset Name;###setcounter###_of_###numberofsets###_###FN###_###SN###
FN;Steve;Paul;Jack;July;Christin;Mary;Anne;Joe;Myriam;Timothy
SN;Bergman;Cook;Carpenter;Wolf;Shapiro;Kovalsky;Witbury;Omar
Married;Yes;No
Children;;1;2;3;4;5;6;7;8;9;10
run ID;###constantuuid###
set ID;###datasetuuid###
Run Timestamp;###timestamp###
Set Timestamp;###datasettimestamp###
```
Each line in the dgschema file consists of a name followed by a list of possible values. 
Each name will be translated to a column name in the resulting output file. Each cell value will be selected
randomly from the list of possible values.<br>
<br>
**Predefined values for replacement:**<br>
|Value|Description|
|---|---|
|**###setcounter###**|Contains the current dataset number|
|**###numberofsets###**|Number of datasets to be generated|
|**###delimiter###**|Delimiter between values|
|**###time###**|Current time|
|**###date###**|Current date|
|**###datetime###**|Current datetime|
|**###timestamp###**|Current timestamp|
|**###datasettimestamp###**|Timestamp per dataset|
|**###username###**|Current username (login user)|
|**###constantuuid###**|Unique Identifier for this run|
|**###datasetuuid###**|Unique Identifier for current dataset|

Each value of the current dataset can be referenced for replacement
in the same line (see exmaple above).<br>
<br>
**Possible generation parameters:**<br>
|Value|Description|
|-|-|
|**numberofsets**|How many datasets will be generated (100 default)|
|**delimiter**|delimiter between values, ; (semicolon) is default|
|**replacerkey**|Marker string for value replacements, usually ###|
|**useheader**|Y (default) oder N for header line or no header line|
|**username**|Current username depending on operating system login|
|**timeformat**|Format string for time values|
|**dateformat**|Format string for date values|
|**datetimeformat**|Format string for datetime values|
|**timestampformat**|Format string for timestamp values|
|**templatefilename**|Template filename for single file per dataset|
|**outputfilename**|Output filename|
|**appendtofile**|N (default) or Y for overwriting or appending|

Generation parameters can be set in the dgschema file via !!! Annotation (see example above).<br>
Generation parameters are optional.<br>

Template engine
-
A template file can be addressed via templatefilename for outputs like XML or JSON.
The template file may contain column references in the usual style (###column name###). Example:<br>
**dgschema file:**<br>
```
!!!numberofsets;1000
!!!delimiter;";"
!!!useheader;N
!!!templatefilename;person.jsontemplate
!!!outputfilename;person_###setcounter###.json

Dataset Name;###setcounter###_of_###numberofsets###_###FN###_###SN###
FN;Steve;Paul;Jack;July;Christin;Mary;Anne;Joe;Myriam;Timothy
SN;Bergman;Cook;Carpenter;Wolf;Shapiro;Kovalsky;Witbury;Omar
Married;Yes;No
Children;;1;2;3;4;5;6;7;8;9;10
run ID;###constantuuid###
set ID;###datasetuuid###
Run Timestamp;###timestamp###
Set Timestamp;###datasettimestamp###
```
**person.jsontemplate:**<br>
```
{
    "id": "###datasetuuid###",
    "First Name": "###FN###",
    "Surname": "###SN###",
    "Married": "###Married###",
    "Children": "###Children###"
}
```
